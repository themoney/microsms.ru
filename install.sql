CREATE SCHEMA IF NOT EXISTS `microsms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `microsms` ;

-- -----------------------------------------------------
-- Table `microsms`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`groups` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`groups` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `procent` FLOAT(4,2) NOT NULL ,
  `access` INT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`users` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `group_id` INT NOT NULL DEFAULT 1 ,
  `login` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `balance` FLOAT(8,2) NOT NULL ,
  `session` CHAR(32) NOT NULL ,
  `registration` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' ,
  `lastvisit` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `user_group` (`group_id` ASC) ,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) ,
  CONSTRAINT `user_group`
    FOREIGN KEY (`group_id` )
    REFERENCES `microsms`.`groups` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`news`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`news` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(45) NOT NULL ,
  `text` TEXT NOT NULL ,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`pages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`pages` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`pages` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `title` VARCHAR(45) NOT NULL ,
  `text` TEXT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`users_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`users_data` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`users_data` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `value` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `data_user` (`user_id` ASC) ,
  CONSTRAINT `data_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`sms_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`sms_status` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`sms_status` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `status` VARCHAR(45) NOT NULL ,
  `desc` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `status_UNIQUE` (`status` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`services_smsin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`services_smsin` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`services_smsin` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `type` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `desc` VARCHAR(45) NOT NULL ,
  `skey` VARCHAR(45) NOT NULL ,
  `url` VARCHAR(150) NOT NULL ,
  `encode` ENUM('windows-1251','utf-8') NOT NULL DEFAULT 'utf-8' ,
  `status` VARCHAR(45) NOT NULL ,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `smsin_user` (`user_id` ASC) ,
  CONSTRAINT `smsin_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`payments` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`payments` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `category` TINYINT NOT NULL ,
  `user_id` INT NOT NULL ,
  `desc` VARCHAR(45) NOT NULL ,
  `cost` FLOAT(8,2) NOT NULL ,
  `status` TINYINT NOT NULL ,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `payment_user` (`user_id` ASC) ,
  CONSTRAINT `payment_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`countries` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`countries` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`numbers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`numbers` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`numbers` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `country_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `number_country` (`country_id` ASC) ,
  CONSTRAINT `number_country`
    FOREIGN KEY (`country_id` )
    REFERENCES `microsms`.`countries` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`prefix__1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`prefix__1` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`prefix__1` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `number_id` INT NOT NULL ,
  `prefix` VARCHAR(45) NOT NULL ,
  `available` TINYINT(1)  NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `prefix_number` (`number_id` ASC) ,
  CONSTRAINT `prefix_number`
    FOREIGN KEY (`number_id` )
    REFERENCES `microsms`.`numbers` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`prefix__2`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`prefix__2` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`prefix__2` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `service_id` INT NOT NULL ,
  `prefix_id` INT NOT NULL ,
  `prefix` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `prefix_prefix1` (`prefix_id` ASC) ,
  INDEX `prefix_user` (`user_id` ASC) ,
  INDEX `prefix_service` (`service_id` ASC) ,
  CONSTRAINT `prefix_prefix1`
    FOREIGN KEY (`prefix_id` )
    REFERENCES `microsms`.`prefix__1` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `prefix_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `prefix_service`
    FOREIGN KEY (`service_id` )
    REFERENCES `microsms`.`services_smsin` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`operators`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`operators` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`operators` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `country_id` INT NOT NULL ,
  `code` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `priority` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `operator_country` (`country_id` ASC) ,
  CONSTRAINT `operator_country`
    FOREIGN KEY (`country_id` )
    REFERENCES `microsms`.`countries` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`summary_stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`summary_stats` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`summary_stats` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `service_id` INT NOT NULL ,
  `prefix_id` INT NOT NULL ,
  `country_code` VARCHAR(45) NOT NULL ,
  `operator_code` VARCHAR(45) NOT NULL ,
  `number` INT NOT NULL ,
  `status` VARCHAR(45) NOT NULL ,
  `abonent` VARCHAR(45) NOT NULL ,
  `cost` FLOAT(8,2) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `summary_user` (`user_id` ASC) ,
  INDEX `summary_service` (`service_id` ASC) ,
  INDEX `summary_prefix` (`prefix_id` ASC) ,
  INDEX `summary_country` (`country_code` ASC) ,
  INDEX `summary_status` (`status` ASC) ,
  CONSTRAINT `summary_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `summary_service`
    FOREIGN KEY (`service_id` )
    REFERENCES `microsms`.`services_smsin` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `summary_prefix`
    FOREIGN KEY (`prefix_id` )
    REFERENCES `microsms`.`prefix__2` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `summary_country`
    FOREIGN KEY (`country_code` )
    REFERENCES `microsms`.`countries` (`code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `summary_status`
    FOREIGN KEY (`status` )
    REFERENCES `microsms`.`sms_status` (`status` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`details_stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`details_stats` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`details_stats` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `smsid` VARCHAR(45) NOT NULL ,
  `date_received` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' ,
  `date_response` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' ,
  `country_code` VARCHAR(45) NOT NULL ,
  `operator_code` VARCHAR(45) NOT NULL ,
  `mtt` VARCHAR(45) NOT NULL ,
  `service_id` INT NOT NULL ,
  `number` VARCHAR(45) NOT NULL ,
  `abonent` VARCHAR(45) NOT NULL ,
  `prefix_id` INT NOT NULL ,
  `input` TEXT NOT NULL ,
  `output` TEXT NOT NULL ,
  `response_server` TEXT NOT NULL ,
  `status` VARCHAR(45) NOT NULL ,
  `status_payment` VARCHAR(45) NOT NULL ,
  `cost_partner` FLOAT(8,2) NOT NULL ,
  `cost` FLOAT(8,2) NOT NULL ,
  `user_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `smsid_UNIQUE` (`smsid` ASC) ,
  INDEX `stats_service` (`service_id` ASC) ,
  INDEX `stats_user` (`user_id` ASC) ,
  CONSTRAINT `stats_service`
    FOREIGN KEY (`service_id` )
    REFERENCES `microsms`.`services_smsin` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `stats_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`tariffs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`tariffs` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`tariffs` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `operator_id` INT NOT NULL ,
  `number` INT NOT NULL ,
  `abonent_price` FLOAT(8,4) NOT NULL ,
  `abonent_currency` VARCHAR(8) NOT NULL ,
  `abonent_price_rur` FLOAT(8,4) NOT NULL ,
  `partner_income` FLOAT(8,4) NOT NULL ,
  `partner_currency` VARCHAR(8) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `operator_id_UNIQUE` (`operator_id` ASC) ,
  INDEX `tariffs_operator` (`operator_id` ASC) ,
  CONSTRAINT `tariffs_operator`
    FOREIGN KEY (`operator_id` )
    REFERENCES `microsms`.`operators` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`black_prefix`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`black_prefix` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`black_prefix` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `prefix` VARCHAR(45) NOT NULL ,
  `comment` VARCHAR(150) NOT NULL ,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `user_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `black_user` (`user_id` ASC) ,
  CONSTRAINT `black_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `microsms`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `microsms`.`prefix_available`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `microsms`.`prefix_available` ;

CREATE  TABLE IF NOT EXISTS `microsms`.`prefix_available` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `service_id` INT NOT NULL ,
  `prefix_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `available_service` (`service_id` ASC) ,
  INDEX `available_prefix` (`prefix_id` ASC) ,
  CONSTRAINT `available_service`
    FOREIGN KEY (`service_id` )
    REFERENCES `microsms`.`services_smsin` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `available_prefix`
    FOREIGN KEY (`prefix_id` )
    REFERENCES `microsms`.`prefix__1` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Data for table `microsms`.`groups`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `microsms`;
INSERT INTO `microsms`.`groups` (`id`, `name`, `procent`, `access`) VALUES (1, 'Неактивный', 0, 0);
INSERT INTO `microsms`.`groups` (`id`, `name`, `procent`, `access`) VALUES (2, 'Заблокированный', 0, 0);
INSERT INTO `microsms`.`groups` (`id`, `name`, `procent`, `access`) VALUES (3, 'Партнёр', 90, 10);
INSERT INTO `microsms`.`groups` (`id`, `name`, `procent`, `access`) VALUES (4, 'Менеджер', 99, 90);
INSERT INTO `microsms`.`groups` (`id`, `name`, `procent`, `access`) VALUES (5, 'Администратор', 100, 100);

COMMIT;

-- -----------------------------------------------------
-- Data for table `microsms`.`users`
-- -----------------------------------------------------
SET AUTOCOMMIT=0;
USE `microsms`;
INSERT INTO `microsms`.`users` (`id`, `group_id`, `login`, `password`, `balance`, `session`, `registration`, `lastvisit`) VALUES (1, 5, 'admin', 'pass', 0, '', '', '');
INSERT INTO `microsms`.`users` (`id`, `group_id`, `login`, `password`, `balance`, `session`, `registration`, `lastvisit`) VALUES (2, 1, 'test', 'test', 0, '', '', '');
INSERT INTO `microsms`.`users` (`id`, `group_id`, `login`, `password`, `balance`, `session`, `registration`, `lastvisit`) VALUES (3, 2, 'bad', 'bad', 0, '', '', '');
INSERT INTO `microsms`.`users` (`id`, `group_id`, `login`, `password`, `balance`, `session`, `registration`, `lastvisit`) VALUES (4, 3, 'demo', 'demo', 0, '', '', '');
INSERT INTO `microsms`.`users` (`id`, `group_id`, `login`, `password`, `balance`, `session`, `registration`, `lastvisit`) VALUES (5, 4, 'manager', 'manager', 0, '', '', '');

COMMIT;
