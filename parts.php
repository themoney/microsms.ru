#!/usr/bin/php5 -f
<?php
$table = "details_stats";
$column = "date_received";
$date1 = "2011-01-01 00:00:00";
$date2 = "2012-12-31 00:00:00";

function get_part($i) {
    if($i < 10) $part = '00' . $i;
    if($i >= 10 && $i < 100) $part = '0' . $i;
    if($i >= 100 && $i < 1000) $part = $i;
    #if($i >= 1000) $part = $i;
    return $part;
}

if($date1 < $date2) {
    $dates_range[] = $date1;
    $date1 = strtotime($date1);
    $date2 = strtotime($date2);
    $sql = "ALTER TABLE `" . $table . "` PARTITION BY RANGE (UNIX_TIMESTAMP(`" . $column . "`))\n(\n";
    $parts = array();
    $date1 = mktime(0, 0, 0, date("m", $date1), date("d", $date1), date("Y", $date1));
    $parts[] = "PARTITION p" . get_part(count($parts)+1) . " VALUES LESS THAN (UNIX_TIMESTAMP('" . date('Y-m-d H:i:s', $date1) . "'))";
    while ($date1 != $date2) {
        $date1 = mktime(0, 0, 0, date("m", $date1), date("d", $date1)+1, date("Y", $date1));
        $parts[] = "PARTITION p" . get_part(count($parts)+1) . " VALUES LESS THAN (UNIX_TIMESTAMP('" . date('Y-m-d H:i:s', $date1) . "'))";
    }
    $parts[] = "PARTITION p" . get_part(count($parts)+1) . " VALUES LESS THAN (MAXVALUE)";
    if(count($parts) >= 1024) exit('Parts over of limit 1024');
    $sql .= implode(",\n", $parts);
    $sql .= "\n);";
    echo $sql;
}
