<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAppAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'App',
            'basePath'  => dirname(__FILE__),
        ));
        return $autoloader;
    }

    /*protected function _initAutoLoad()
    {
        $moduleLoader = new Zend_Application_Module_Autoloader(
            array('namespace'=>'Panel','basePath'=>APPLICATION_PATH . '/modules/panel'),
            array('namespace' => 'Default','basePath' => APPLICATION_PATH . '/modules/default')
        );
        #$autoload = Zend_Loader_Autoloader::getInstance();
        #Zend_Debug::dump($autoload->getRegisteredNamespaces());
        #$moduleLoader->setResourceTypes()
        Zend_Debug::dump($moduleLoader->getNamespace()); exit();
        return $moduleLoader;
    }

    protected function _initSession()
    {
        Zend_Session::start();
        #Zend_Session::rememberMe(3600);
        Zend_Registry::set('session', new Zend_Session_Namespace('Default'));
    }

    /*protected function _initConfig()
    {
        Zend_Registry::set('config', new Zend_Config($this->getOptions()));
    }*/

    protected function _initCache()
    {
        $options = $this->getOptions();
        $cache = Zend_Cache::factory(
            $options['cache']['frontend']['adapter'],
            $options['cache']['backend']['adapter'],
            $options['cache']['frontend']['params'],
            $options['cache']['backend']['params']
        );
        if((bool)$options['cache']['isDefaultMetadataCache']) Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        if((bool)$options['cache']['isDefaultTranslateCache']) Zend_Translate::setCache($cache);
        if((bool)$options['cache']['isDefaultLocaleCache']) Zend_Locale::setCache($cache);
        if((bool)$options['cache']['isRegistrySet']) Zend_Registry::set('cache', $cache);
        return $cache;
    }

    protected function _initDb()
    {
        #Zend_Debug::dump(Zend_Loader_Autoloader::getInstance()->getAutoloaders());
        #exit;
        try {
            $config = $this->getOptions();
            #Zend_Debug::dump($config);
            #exit();
            $db = Zend_Db::factory($config['database']['adapter'], $config['database']['params']);
            Zend_Db_Table::setDefaultAdapter($db);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        Zend_Registry::set('db', $db);
        return $db;
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('Email', $message, Zend_Wildfire_Plugin_FirePhp::WARN);
    }

    protected function _initLogger()
    {
        try {
            $this->bootstrap('db');
            $logger = new Zend_Log();
            $file = new Zend_Log_Writer_Stream(APPLICATION_PATH . "/../logs/errors.log");
            $file->setFormatter(new Zend_Log_Formatter_Simple('%timestamp% %priorityName% (%priority%): %message%' . PHP_EOL));
            $logger->addWriter($file);
            $firebug = new Zend_Log_Writer_Firebug();
            $logger->addWriter($firebug);
            $columnMapping = array(
                'priority'     => 'priority',
                'priorityName' => 'priorityName',
                'message'      => 'message',
                'timestamp'    => 'timestamp',
                'ip'           => 'ip',
                'controller'   => 'controller',
                'action'       => 'action'
            );
            $db = new Zend_Log_Writer_Db(Zend_Registry::get('db'), 'logs', $columnMapping);
            $logger->addWriter($db);
            Zend_Registry::set("log", $logger);
            #Zend_Controller_Action_HelperBroker::addPrefix("Core_Controller_Action_Helper");
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    /*public function _initZendValidateTranslator() {
        $translator = new Zend_Translate(
            'array',
            APPLICATION_PATH . '/../resources/languages',
            'ru',
            array('scan' => Zend_Translate::LOCALE_DIRECTORY)
        );
        Zend_Validate_Abstract::setDefaultTranslator($translator);
        return $translator;
    }

    protected function _initDoctype()
    {
        #define('VER', 0.1000);
        $config = $this->getOptions();
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype($config['resources']['layout']['doctype']);
        $view->headTitle()->setSeparator($config['resources']['view']['titleSeparator'])->set($config['resources']['view']['title']);
        $view->headMeta()->appendName('description', $config['resources']['view']['description']);
        $view->headMeta()->appendName('keywords', $config['resources']['view']['keywords']);
        $view->headMeta()->appendHttpEquiv('Content-Type', $config['resources']['view']['contentType']);
        $view->headMeta()->appendHttpEquiv('X-UA-Compatible','IE=EmulateIE7');
        #$view->headLink()->appendStylesheet('http://cashexpress.me/css/style.css?v=' . VER);
        $view->headLink()->appendStylesheet('/css/styles.css');
        #$view->headLink()->appendStylesheet('/css/compassdatagrid.css');
        $view->headScript()->appendFile('https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js');
        #$view->headScript()->appendFile('/js/jquery.livequery.min.js');
        #$view->headScript()->appendFile('/js/jquery.compassdatagrid.min.js');
        #$view->headScript()->appendFile('/js/jquery.detailsRow.js');
        #$view->headScript()->appendFile('/js/functions.js');
        return $view;
    }

    protected function _initDevelopmentBar()
    {
    	try {
		    if(APPLICATION_ENV !== 'development') return;
        	$autoloader = Zend_Loader_Autoloader::getInstance();
	        $autoloader->registerNamespace('Core');
            $frontController = Zend_Controller_Front::getInstance();
            $frontController->registerPlugin(new Core_Controller_Plugin_Developmentbar());
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }*/
}
