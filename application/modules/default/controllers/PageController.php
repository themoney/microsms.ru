<?php
class Default_PageController extends Zend_Controller_Action
{

    public function __call($page, $args) {
        require_once 'Zend/Controller/Action/Exception.php';
        try {
            $page = $this->_getParam('page', substr($page, 0, strlen($page) - 6));
            echo $this->render($page);
        } catch(Zend_Exception $e) {
            throw new Zend_Controller_Action_Exception(sprintf('Страница "%s" не найдена', $page), 404);
        }
    }

    public function aboutAction() {
        $this->view->headTitle('test');
        #$acl = Zend_Registry::get('acl');
        #Zend_Debug::dump($acl->is);
        #echo '';
    }

}

