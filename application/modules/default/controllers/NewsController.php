<?php

class Default_NewsController extends Zend_Controller_Action
{
    protected $news;

    public function init()
    {
        $this->view->headTitle('Новости');
        $this->news = new Default_Model_News();
    }

    public function indexAction()
    {
        if($this->_hasParam('page')) {
            $page = $this->_getParam('page', 1);
            $this->view->headTitle()->append('Страница ' . $page);
        }
        $this->news->view();
    }

}

