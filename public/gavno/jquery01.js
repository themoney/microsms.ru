﻿var randomstr = "";
var wait_msg = "Ждать осталось ";
var minutes = ["минут","минуты","минуту"]
var seconds = ["секунд","секунды","секунду"];
var warn_msg = "Идет закачка. Хотите остановить?";
var glob_progress_url = "";
var glob_progress_key = "";
var progress = new Object;
var progress_time;

var g_forms = new Array();
var g_form_counter = 0;
var g_form_count = 0;
var g_form_current = new Object;
function clean(){}

$().ready(function(){
	window.setTimeout(function(){
	if (!$("body").hasClass("auth"))
	{
		cleanIfNotEmpty(['l', 's']);
	}
	if ($("body").hasClass("auth"))
	{

	$(".more").click(function(){
		var count = 5-$("input[@type=file][value='']").size();
		while (count>0)
		{
			$(".file-last").removeClass("file-last").after('<div class="input file-last b-input-file"><input type="file" name="file" /><label class="b-file-upper b-file-noready">&nbsp;</label></div>');
			count--;
		}
		$("input[@type=file]").unbind("change").unbind("keyup").bind("change",function(){fcheck()}).bind("keyup",function(){fcheck()});
		return false;
	});
	$("#agreement").attr("checked","checked");
	$("#agreements").attr("checked","checked");

	$("#b-button-upload").addClass("b-button-green4").attr("href","#");
	$("#b-button-upload").removeClass("b-button-gray6");

	$("#agreement").click(function(){
		if ($(this).attr("checked") && g_upload_progress == false)
		{
			$("#b-button-upload").addClass("b-button-green4").attr("href","#");
			$("#b-button-upload").removeClass("b-button-gray6");
			fcheck();
		}
		else{
			$("#b-button-upload").removeClass("b-button-green4").removeAttr("href");
			$("#b-button-upload").addClass("b-button-gray6");
		}
	});


	$("#agreements").click(function(){
		if ($(this).attr("checked"))
		{
			var url = $(".b-site-create").attr("href2");
			$(".b-site-create").addClass("b-button-orange3").attr("href",url);
			$(".b-site-create").removeClass("b-button-gray8");
		}
		else{
			var url = $(".b-site-create").attr("href");
			$(".b-site-create").attr("href2",url);
			$(".b-site-create").removeClass("b-button-orange3").removeAttr("href");
			$(".b-site-create").addClass("b-button-gray8");
		}
	});

	$("#b-button-upload").dblclick(function(){return false});
	$("#b-button-upload").click(function(){
		if ($(this).hasClass("b-button-green4") && g_upload_progress == false){
			g_upload_progress = true;
			$("input[@type=file][value='']").parent().remove();
			//var el = $(".b-input-file form input[@type=file][value!='']")
			//var el = $(".b-input-file form .b-file-noready").removeClass("b-file-noready");
			var el = $(".file-inputs");
			g_forms = $(el).parent();
			g_form_count = $(el).size();
			getform();
		}
		return false;
	});

	$("input[@type=file]").bind("change",
	    function(){
		fcheck()
	}).bind("keyup",
	    function(){
		fcheck()
    });
    	fcheck();
	}
	$(".b-more").click(function(){
		b_more();
		return false;
	});
	
	$(".b-inplace-action-dot").click(function(){
		b_more();
		return false;
	});
	
	
 },100);
});



var imgIndicator = new Object();
var imgCount = 6;
var imgRepeat = 2;
var counts = 0;
var img_need = false;
for (var j = 0; j < imgRepeat; j++ ) {
    for (var i = 1; i <= imgCount; i++ ) {
        imgIndicator['img' + (i + j * imgCount)] = new Image(16,16);
    }
}
imgIndicator['img' + (imgRepeat * imgCount + 1)] = new Image(16,16);

var g_txt_path = "/disk/more/m%num%/";
var g_txt_max = 2;
var g_txt_current = 0;
var g_upload_progress = false;
var g_upload_files = 0;
var g_p = 0;

function _cp()
{
	if ($("#s").val())
	{
		$("#s").prev().css("top","-9999px");
	}
}
function cleanIfNotEmpty(list)
{
	$.each(list,function(i,n){
		var a = $("#"+n);
		var p = a.prev();
		var n;
		if (!$(a).val())
		{
			$(p).css("top","");
		}
		else{
			$(p).css("top","-9999px");
		}
		$(a).blur(function() {
			g_p=0;
			if (!$(a).val()) {
				$(p).css("top","");
			}
		}).focus(function(){
			g_p=1;
			$(p).css("top","-9999px");
		}).click(function(){
			if (!$(a).val() && g_p==0) {
				$(p).css("top","");
			}
			return true;
		});

	});
	setInterval("_cp()",100);
}
