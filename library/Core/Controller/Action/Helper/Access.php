<?php
/**
 * Created by JetBrains PhpStorm.
 * User: intech
 * Date: 11.03.11
 * Time: 3:13
 * To change this template use File | Settings | File Templates.
 */

class Core_Controller_Action_Helper_Access extends Zend_Controller_Action_Helper_Abstract
{
    protected $_action;
    protected $_auth;
    protected $_acl;
    protected $_controllerName;

    public function __construct(Zend_View_Interface $view = null, array $options = array()) {
        $this->_auth = Zend_Auth::getInstance();
        $this->_acl = $options['acl'];
    }

    /**
     * Hook into action controller initialization
     * @return void
     */
    public function init() {
        $this->_action = $this->getActionController();
        // add resource for this controller
        $controller = $this->_action->getRequest()->getControllerName();
        if (!$this->_acl->has($controller)) {
            $this->_acl->add(new Zend_Acl_Resource($controller));
        }
    }

    /**
     * Proxy to the underlying Zend_Acl's allow() function.
     *
     * We use the controller's name as the resource and the
     * action name(s) as the privilege(s)
     *
     * @param Zend_Acl_Role_Interface|string|array    $roles
     * @param string|array    $actions
     * @uses    Zend_Acl::setRule()
     * @return Places_Controller_Action_Helper_Acl Provides a fluent interface
     */
    public function allow($roles = null, $actions = null) {
        $resource = $this->_controllerName;
        $this->_acl->allow($roles, $resource, $actions);
        return $this;
    }

    /**
     * Proxy to the underlying Zend_Acl's deny() function.
     *
     * We use the controller's name as the resource and the
     * action name(s) as the privilege(s)
    Please post comments or corrections to the Author Online forum at
    http://www.manning-sandbox.com/forum.jspa?forumID=329
    Licensed to Menshu You <dollequatki@gmail.com>
     *
     * @param Zend_Acl_Role_Interface|string|array    $roles
     * @param string|array    $actions
     * @uses    Zend_Acl::setRule()
     * @return Places_Controller_Action_Helper_Acl Provides a fluent interface
     */
    public function deny($roles = null, $actions = null) {
        $resource = $this->_controllerName;
        $this->_acl->allow($roles, $resource, $actions);
        return $this;
    }

    public function preDispatch() {
        #Zend_Debug::dump($this->_auth->getIdentity());
        #if(!$this->_acl->isAllowed($this->_auth->getIdentity()->name, $this->_controllerName, $this->_action)) {
            new Zend_Exception('Access Denied', 500);
        #}
    }
}
