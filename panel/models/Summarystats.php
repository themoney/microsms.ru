<?php

class Core_Model_Summarystats extends Zend_Db_Table_Abstract
{
    protected $_name = 'summary_stats';

    protected $user_id = null;

    protected $filter = array();

    public function init() {

    }

    public function view() {
        #$this->setUser($user_id);
        $select = $this->select()
                ->from($this->_name, array('date', 'cost' => new Zend_Db_Expr("sum(`cost`)"), 'ok' => new Zend_Db_Expr("sum(`ok`)"), 'error' => new Zend_Db_Expr("sum(`error`)")))
                ->group("date")
                ->where("user_id = ?", $this->user_id);
        if (!empty($this->filter)) {
            foreach($this->filter as $field => $value) {
                $select->where($field, $value);
            }
        }
        $sum = 0;
        $ok = 0;
        $error = 0;
        $result = array();
        try {
        $rowset = $this->fetchAll($select);
        foreach ($rowset as $row)
        {
            $row['date'] = date('d-m-Y', strtotime($row['date']));
            $result[] = $row;
            $sum += $row['cost'];
            $ok += $row['ok'];
            $error += $row['error'];
        }
        } catch (Exception $e) {
            exit('ERROR: ' . $e->getMessage());
        }
        /*$date = strtotime('2011-01-01 00:00:00');
        for($i=0;$i<30;$i++) {
            $date = mktime(0, 0, 0, date("m", $date), date("d", $date)+1, date("Y", $date));
            $result[$i] = array('date' => date('Y-m-d', $date), 'cost' => rand(4000, 10000000), 'ok' => rand(1, 1000), 'error' => rand(0, 100));
            $sum += rand(4000, 10000000);
            $ok += rand(1, 1000);
            $error += rand(0, 100);
        }*/
        return array('count' => array('sum' => $sum, 'ok' => $ok, 'error' => $error), 'result' => $result);
    }

    public function getStats($type) {
        $this->setUser(1);
        switch($type) {
            case 'today':
                $date = new Zend_Date();
                $from = $date->toString('YYYY-MM-dd');
                $date->sub(1, Zend_Date::DAY_SHORT);
                $to = $date->toString('YYYY-MM-dd');
                $select = $this->select()
                    ->from($this->_name, array('cost' => new Zend_Db_Expr("sum(`cost`)")))
                    ->where("date BETWEEN TIMESTAMP(?) AND TIMESTAMP(?)", $from, $to)
                    ->where("user_id = ?", $this->user_id);
                $row = $this->fetchRow($select);
                return round($row->cost, 2);
            break;

            case 'yesterday':
                $date = new Zend_Date();
                $date->sub(1, Zend_Date::DAY_SHORT);
                $from = $date->toString('YYYY-MM-dd');
                $date->sub(2, Zend_Date::DAY_SHORT);
                $to = $date->toString('YYYY-MM-dd');
                $select = $this->select()
                    ->from($this->_name, array('cost' => new Zend_Db_Expr("sum(`cost`)")))
                    ->where("date BETWEEN TIMESTAMP(?) AND TIMESTAMP(?)", $from, $to)
                    ->where("user_id = ?", $this->user_id);
                $row = $this->fetchRow($select);
                return round($row->cost, 2);
            break;

            case 'week':
                $date = new Zend_Date();
                $from = $date->toString('YYYY-MM-dd');
                $date->sub(1, Zend_Date::WEEK);
                $to = $date->toString('YYYY-MM-dd');
                $select = $this->select()
                    ->from($this->_name, array('cost' => new Zend_Db_Expr("sum(`cost`)")))
                    ->where("date BETWEEN TIMESTAMP(?) AND TIMESTAMP(?)", $from, $to)
                    ->where("user_id = ?", $this->user_id);
                $row = $this->fetchRow($select);
                return round($row->cost, 2);
            break;

            case 'lastweek':
                $date = new Zend_Date();
                $date->sub(1, Zend_Date::WEEK);
                $from = $date->toString('YYYY-MM-dd');
                $date->sub(2, Zend_Date::WEEK);
                $to = $date->toString('YYYY-MM-dd');
                $select = $this->select()
                    ->from($this->_name, array('cost' => new Zend_Db_Expr("sum(`cost`)")))
                    ->where("date BETWEEN TIMESTAMP(?) AND TIMESTAMP(?)", $from, $to)
                    ->where("user_id = ?", $this->user_id);
                $row = $this->fetchRow($select);
                return round($row->cost, 2);
            break;

            default:
                return 0;
            break;
        }
    }

    public function setFilter($field, $value) {
        $this->filter[$field] = $value;
    }

    public function setUser($user_id) {
        $this->user_id = $user_id;
    }

    public function getForm() {
        $form = new Core_Form_Stats();
        $form->initForm($this->user_id);
        return $form;
    }

}