<?php

class Core_Model_News extends Zend_Db_Table_Abstract
{
    protected $_name = 'news';

    protected $limitPerPage = 5;

    public function init() {
                
    }

    /*
     * Получение последних новостей
     * @param int $count - кол-во последних новостей
     * @return class rowset
     */
    public function last($count = 5) {
        return $this->fetchAll(null, 'id DESC', $count);
    }

    /*
     * Получение новостей постранично
     * @param int $page - номер страницы
     * @return array news
     */
    public function page($page = 1) {
        $adapter = new Zend_Paginator_Adapter_DbSelect($this->select()->order('id DESC'));
        $paginator = new Zend_Paginator($adapter);
        $paginator->setItemCountPerPage($this->limitPerPage);
        $paginator->setCurrentPageNumber($page);
        #Zend_Debug::dump($paginator); exit();
        return $paginator;
    }

    /*
     * Получение новости по id
     * @param int $id - id новости
     * @return object item news
     */
    public function item($id = 1) {
        return $this->fetchRow($this->select()->where('id = ?', $id));
    }
}