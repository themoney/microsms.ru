<?php
/*
       SOAP Agregator
       1.00
       20.07.2008
*/
class Core_Model_PayStream extends Zend_Soap_Client {

    public function init(){
        ini_set('soap.wsdl_cache_enabled', false);
        $this->setWsdl('http://api.paystream.ru/api.wsdl');
        $this->setHttpLogin('ssi');
        $this->setHttpPassword('1saq01frt2');
        $this->setWsdlCache(false);
    }

    public function get($name, $argument=array()){
        return $this->__call($name,$argument);
    }
}