<?php

class Core_Model_Detailstats extends Zend_Db_Table_Abstract
{
    protected $_name = 'details_stats';

    protected $user_id = null;

    protected $filter = array();

    protected $limitPerPage = 25;

    public function init() {

    }

    public function view($page = 1, $sortField = 'date_received', $sortDir = 'DESC') {
        #$this->setUser($user_id);
        $select = $this->select()
                ->from($this->_name)
                ->order($sortField . " " . $sortDir)
                ->where("user_id = ?", $this->user_id);
        if (!empty($this->filter)) {
            foreach($this->filter as $field => $value) {
                $select->where($field, $value);
            }
        }
        #exit($select->__toString());
        $adapter = new Zend_Paginator_Adapter_DbSelect($select);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setItemCountPerPage($this->limitPerPage);
        $paginator->setCurrentPageNumber($page);
        return $paginator;
    }

    public function getId($id) {
        $select = $this->select()
                ->from(array('a' => $this->_name))
                //->joinLeft(array("p" => "prefix__2"), "p.id = a.prefix_id", array('prefix' => new Zend_Db_Expr('CONCAT((SELECT `prefix__1`.`prefix` FROM `prefix__1` WHERE `prefix__1`.`id` = p.`prefix_id`), p.prefix)')))
                ->joinLeft(array("s" => "services_smsin"), "s.id = a.service_id", array('service' => 'name'))
                ->joinLeft(array("c" => "countries"), "c.id = a.country_id", array('country' => 'name'))
                ->joinLeft(array("o" => "operators"), "o.id = a.operator_id", array('operator' => 'name'))
                ->joinLeft(array("st" => "sms_status"), "st.status = a.status", array('status' => 'desc'))
                ->where("a.id = ?", (int)$id)
                ->where("a.user_id = ?", $this->user_id);
        $select->setIntegrityCheck(false);
        return $this->fetchRow($select);
    }

    public function setFilter($field, $value) {
        $this->filter[$field] = $value;
    }

    public function setUser($user_id) {
        $this->user_id = $user_id;
    }

    public function getUser() {
        return $this->user_id;
    }

    public function getForm() {
        $form = new Core_Form_Stats();
        $form->initForm($this->user_id, true);
        return $form;
    }

}
