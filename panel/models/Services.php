<?php

class Core_Model_Services extends Zend_Db_Table_Abstract
{
    protected $_name = 'services_smsin';

    protected $user_id = null;

    public function init() {
        
    }

    public function view($sortField, $sortDir) {
        $select = $this->select()
                ->from($this->_name, array('id','name','desc','url','status','date'))
                ->order($sortField . " " . $sortDir)
                ->where("user_id = ?", $this->user_id);
        $result = array();
        try {
            $rowset = $this->fetchAll($select);
            foreach ($rowset as $row) $result[] = $row;
        } catch (Exception $e) {
            exit('ERROR: ' . $e->getMessage());
        }
        return $result;
    }

    public function getForm($edit = false) {
        $form = new Core_Form_Services();
        $form->initForm($this->user_id, $edit);
        return $form;
    }

    public function setUser($user_id) {
        $this->user_id = $user_id;
    }

    public function remove($id) {
        return $this->delete(array('id = ?' => $id, 'user_id = ?' => $this->user_id));
    }

}

