<?php
/**
 * Created by JetBrains PhpStorm.
 * User: intech
 * Date: 18.03.11
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
 
class SoapController extends Zend_Controller_Action
{
    
    protected $api;

    protected $db;

    public function init()
    {
        ini_set("memory_limit","128M");
        set_time_limit(0);
        error_reporting(E_ALL);
        #@ob_clean();
        #@ob_end_clean();
        #@ob_end_flush();
        #@ob_flush();
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        $model = new Core_Model_News();
        $this->db = $model->getAdapter()->getConnection();
        $this->api = new Core_Model_Paystream();
        $this->api->init();
    }

    public function updateAction()
    {
        $this->db->exec('TRUNCATE TABLE `tariffs`');
        $this->db->exec('TRUNCATE TABLE `operators`');
        $this->db->exec('TRUNCATE TABLE `countries`');
        $this->db->exec('TRUNCATE TABLE `numbers`');
        echo "<b>Countries...</b><br />\n<pre>"; #flush();

        $countries = $this->api->getCountries();
        #print_r($countries); flush();
        foreach($countries as $country){
            $this->db->exec('INSERT INTO countries (`id`, `code`, `name`) VALUES(' . (int)$country['id'] . ', "' . $country['short_name'] . '", "' . $country['name'] . '")');
            print_r($country); #flush();
        }
        echo "</pre><hr><b>OK</b><br />\n"; #flush();

        echo "<hr><b>Operators...</b><br />\n<pre>"; #flush();
        $operators = $this->api->getOperators();
        $_operators = array();
        foreach($operators as $operator){
            print_r($operator); #flush();
            $this->db->exec('INSERT INTO `operators` (`id`, `country_id`, `code`, `name`, `priority`) VALUES(' . (int)$operator['id'] . ', ' . (int)$operator['country_id'] . ', "' . mysql_escape_string($operator['short_name']) . '", "' . mysql_escape_string($operator['name']) . '", ' . (int)$operator['priority'] . ')');
            $_operators[(int)$operator['id']] = (int)$operator['country_id'];
        }
        echo "</pre><hr><b>OK</b><br />\n"; #flush();

        echo "<hr><b>Tariffs:</b><br />\n<pre>"; #flush();
        $tariffs = $this->api->getTariffs(558);
        #print_r($tariffs); flush();
        echo "</pre><hr><b>Inserts:</b><br />\n<pre>"; #flush();
        $numbers = array();
        foreach($tariffs as $tariff){
	        $this->db->exec('INSERT INTO `tariffs` (`id`, `operator_id`, `number`, `abonent_price`, `abonent_currency`, `abonent_price_rur`, `partner_income`,  `partner_currency`) VALUES (NULL, ' . (int)$tariff['operator_id'] . ', ' . (int)$tariff['num'] . ', ' . (float)$tariff['abonent_price'] . ', "' . mysql_escape_string($tariff['abonent_currency']) . '", ' . (float)$tariff['abonent_price_rur'] . ', ' . (float)$tariff['partner_income'] . ', "' . mysql_escape_string($tariff['partner_currency']) . '")');
	        print_r($tariff); #flush();
            if(!in_array($_operators[(int)$tariff['operator_id']] . '_' . (int)$tariff['num'], $numbers)) {
                $numbers[] = $_operators[(int)$tariff['operator_id']] . '_' . (int)$tariff['num'];
                $this->db->exec('INSERT INTO `numbers` (`id`, `country_id`, `number`) VALUES (NULL, ' . $_operators[(int)$tariff['operator_id']] . ', ' . (int)$tariff['num'] . ')');
            }
            #$this->db->exec('INSERT INTO `prefix_numbers` (`id`, `prefix_id`, `number_id`) VALUES (NULL, ' . $_operators[(int)$tariff['operator_id']] . ', ' . (int)$tariff['num'] . ')');
        }
        echo "</pre><hr><b>OK - " . count($tariffs) . "</b><br />"; #flush();
    }

    public function prefixAction()
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from('numbers');
        $numbers = $db-> fetchAll($select);
        foreach($numbers as $num) {
            $db->insert('prefix__1', array(
                //'number_id' => $num['id'],
                'prefix' => 'bv',
                'available' => 1
            ));
        }
        echo "ok";
    }

    public function statsAction()
    {
        #$this->db->exec('TRUNCATE TABLE `details_stats`');
        #$this->db->exec('TRUNCATE TABLE `summary_stats`');
        #$this->dbd = $this->db;
        #unset($model, $this->db);
        $model = new Core_Model_Summarystats();
        $model_detail = new Core_Model_Detailstats();
        $this->db = $model->getAdapter();
        echo "<b>Stats...</b><br />\n<pre>"; #flush();
        #$date1 = "2010-01-01 00:00:00";
        #$date2 = "2011-01-01 00:00:00";
        #$date1 = strtotime($date1);
        #$date2 = strtotime($date2);
        #while ($date1 != $date2) {
            #$date1 = mktime(0, 0, 0, date("m", $date1), date("d", $date1)+1, date("Y", $date1));
            $stats = $this->api->getStatDate('2011-03-18', 1942);
            #print_r($stats); flush();
            foreach($stats as $row){
                print_r($row); #flush();

                $model_detail->insert(array(
                    'user_id'           => 4,
                    'smsid'             => $row['id'],
                    'date_received'     => $row['date_received'],
                    'date_response'     => $row['date_responsed'],
                    'country_id'        => $row['country_id'],
                    'operator_id'       => $row['operator_id'],
                    'mtt'               => '',
                    'service_id'        => $row['service_id'],
                    'number'            => $row['num'],
                    'abonent'           => $row['abonent'],
                    'prefix'            => $row['prefix'],
                    'input'             => $row['text_in'],
                    'output'            => $row['text_out'],
                    'response_server'   => $row['service_response'],
                    'status'            => $row['status'],
                    'status_payment'    => 'ожидает выплаты',
                    'cost_partner'      => $row['partner_income'],
                    'cost'              => $row['partner_income']
                ));
            }

        $select = $model_detail->select()->from('details_stats', array(
            'date' => new Zend_Db_Expr("DATE_FORMAT(date_received, '%Y-%m-%d')"),
            'service_id',
            'prefix',
            'country_id',
            'operator_id',
            'number',
            'abonent',
            'cost' => new Zend_Db_Expr('sum(cost_partner)'),
            'count' => new Zend_Db_Expr('count(`status`)')
        ))
        ->where('status = ?', 'ok')
        ->group(new Zend_Db_Expr("date"))
        ->group("country_id")
        ->group("operator_id")
        ->group("service_id")
        ->group("number")
        ->group("abonent")
        ->group("prefix");
        #echo $select->__toString();
        #exit(1);
        $all = $model_detail->fetchAll($select);
        foreach($all as $row) {
            $model->insert(array(
                'user_id'       => 4,
                'date'          => $row['date'],
                'service_id'    => $row['service_id'],
                'prefix'        => $row['prefix'],
                'country_id'    => $row['country_id'],
                'operator_id'   => $row['operator_id'],
                'number'        => $row['number'],
                'abonent'       => $row['abonent'],
                'cost'          => $row['cost'],
                'ok'            => $row['count'],
                'error'         => 0
            ));
        }


        $select = $model_detail->select()->from('details_stats', array(
            'date' => new Zend_Db_Expr("DATE_FORMAT(date_received, '%Y-%m-%d')"),
            'service_id',
            'prefix',
            'country_id',
            'operator_id',
            'number',
            'abonent',
            'cost' => new Zend_Db_Expr('sum(cost_partner)'),
            'count' => new Zend_Db_Expr('count(`status`)')
        ))
        ->where('status != ?', 'ok')
        ->group(new Zend_Db_Expr("date"))
        ->group("country_id")
        ->group("operator_id")
        ->group("service_id")
        ->group("number")
        ->group("abonent")
        ->group("prefix");
        #echo $select->__toString();
        #exit(1);
        $all = $model_detail->fetchAll($select);
        foreach($all as $row) {
            $model->insert(array(
                'user_id'       => 4,
                'date'          => $row['date'],
                'service_id'    => $row['service_id'],
                'prefix'        => $row['prefix'],
                'country_id'    => $row['country_id'],
                'operator_id'   => $row['operator_id'],
                'number'        => $row['number'],
                'abonent'       => $row['abonent'],
                'cost'          => $row['cost'],
                'ok'            => 0,
                'error'         => $row['count']
            ));
        }


        #}


        echo "</pre><hr><b>OK</b><br />\n"; #flush();
    }

    public function generatorAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        set_time_limit(0);
        error_reporting(E_ALL);
        $model = new Core_Model_Summarystats();
        $db = $model->getAdapter();
        $countries = $db->fetchAll($db->select()->from('countries'));
        echo "<pre>";
        #print_r($countries->toArray());
        $country = array();
        foreach($countries as $row) {
            $country[$row['id']] = $row;
        }
        #exit();
        $operators = $db->fetchAll($db->select()->from('operators'));
        $operator = array();
        foreach($operators as $row) {
            $operator[$row['id']] = $row;
        }
        $statuses = $db->fetchAll($db->select()->from('sms_status'));
        $status = array();
        foreach($statuses as $row) {
            $status[$row['id']] = $row;
        }
        /*$nums = $db->fetchAll($db->select()->from('numbers'));
        $num = array();
        foreach($nums as $row) {
            $num[$row->id] = $row->toArray();
        }
        $prefixs = $db->fetchAll($db->select()->from('prefix__1'));
        $prefix = array();
        foreach($prefixs as $row) {
            $prefix[$row->id] = $row->toArray();
        }
        $services = $db->fetchAll($db->select()->from('services_smsin'));
        $service = array();
        foreach($services as $row) {
            $service[$row->id] = $row->toArray();
        }*/

        $model_detail = new Core_Model_Detailstats();

        for($i=0;$i<1000;$i++) {
            $model->insert(array(
                'user_id'       => 1,
                'date'          => date('Y-m-d', time()+$i*1500),
                'service_id'    => 1,
                'prefix_id'     => 1,
                'country_code'  => $country[array_rand($country)]['code'],
                'operator_code' => $operator[array_rand($operator)]['code'],
                'number'        => 1234,
                'abonent'       => rand(79010000000,7999999999),
                'cost'          => rand(1.0, 300.50),
                'ok'            => rand(1, 50),
                'error'         => rand(0, 10)
            ));
            $model_detail->insert(array(
                'user_id'           => 1,
                'smsid'             => rand(10000000,9999999999),
                'date_received'     => date('Y-m-d H:i:s', time()+$i*1500+rand(1,100)),
                'date_response'     => date('Y-m-d H:i:s', time()+$i*1650+rand(100,300)),
                'country_code'      => $country[array_rand($country)]['code'],
                'operator_code'     => $operator[array_rand($operator)]['code'],
                'mtt'               => 'MTT',
                'service_id'        => 1,
                'number'            => 1234,
                'abonent'           => rand(79010000000,7999999999),
                'prefix_id'         => 1,
                'input'             => 'abc vasya',
                'output'            => 'Ваш запрос успешно обработан',
                'response_server'   => "this\nhtml\nresponse",
                'status'            => $status[array_rand($status)]['status'],
                'status_payment'    => 'ожидает выплаты',
                'cost_partner'      => rand(1.0, 300.50),
                'cost'              => rand(1.0, 300.50)
            ));
        }
    }

}
