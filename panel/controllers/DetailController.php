<?php

class DetailController extends Zend_Controller_Action
{

    protected $_fieldAlias = array(
        'date'      => 'date_received',
        'operator'  => 'operator_id',
        'country'   => 'country_id',
        'service'   => 'service_id',
        //'prefix'    => 'prefix_id',
        'cost'      => 'cost_partner'
    );

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
        $this->_session = new Zend_Session_Namespace('Form_Stats');
        $this->_model = new Core_Model_Detailstats();
        $this->_acl = Zend_Auth::getInstance();
        $this->_model->setUser((int)$this->_acl->getIdentity()->id);
    }

    protected function buildDate($m, $params) {
        return $params[$m . 'y'] . '-' . $params[$m . 'm'] . '-' . $params[$m . 'd'] . ' ' . (($m=='to') ?  '23:59:59' : '00:00:00');
    }

    protected function alias($field) {
        if(!empty($this->_fieldAlias[$field])) {
            return $this->_fieldAlias[$field];
        }
        return $field;
    }

    protected function filters() {
        if(!empty($this->_session->values['country']))
            $this->_model->setFilter('country_id = ?', $this->_session->values['country']);
        if(!empty($this->_session->values['operator']))
            $this->_model->setFilter('operator_id = ?', $this->_session->values['operator']);
        if(!empty($this->_session->values['number']))
            $this->_model->setFilter('number = ?', $this->_session->values['number']);
        if(!empty($this->_session->values['service']))
            $this->_model->setFilter('service = ?', $this->_session->values['service']);
        if(!empty($this->_session->values['prefix']))
            $this->_model->setFilter('prefix = ?', $this->_session->values['prefix']);
        if(!empty($this->_session->values['abonent']))
            $this->_model->setFilter('abonent LIKE ?', $this->_session->values['abonent'] . "%");
        if(!empty($this->_session->values['textsms']))
            $this->_model->setFilter('input LIKE ?', "%" . $this->_session->values['textsms'] . "%");
        #Zend_Debug::dump(implode(",",$this->_session->values['status']));
        if(!empty($this->_session->values['status']))
            $this->_model->setFilter('status IN(?)', $this->_session->values['status']);

        if(!empty($this->_session->values['datefrom']['fromy'])
            && !empty($this->_session->values['datefrom']['fromm'])
            && !empty($this->_session->values['datefrom']['fromd'])) {
            $from = $this->buildDate('from', $this->_session->values['datefrom']);
            $this->_model->setFilter('date_received >= ?', $from);
        }
        if(!empty($this->_session->values['dateto']['toy'])
            && !empty($this->_session->values['dateto']['tom'])
            && !empty($this->_session->values['dateto']['tod'])) {
            $to = $this->buildDate('to', $this->_session->values['dateto']);
            $this->_model->setFilter('date_received <= ?', $to);
        }
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $form = $this->_model->getForm();
        if(empty($params['status'])) {
            $params['status'] = array_keys($form->status->getMultiOptions());
        }
        $form->status->setValue($params['status']);
        $page = $this->_getParam('page', 1);
        /*switch($params['status']) {
            case 'ok':
                $this->_model->setFilter('status = ?', 'ok');
            break;

            case 'notok':
                $this->_model->setFilter('status != ?', 'ok');
            break;
        }*/
        if ($request->isPost()) {
            $this->_session->values = $params;
            if ($form->isValid($this->_session->values)) {
                $this->filters();
            } else {
                $errors = $form->getErrors();
                foreach($errors as $field => $a) {
                    if(count($a)) $form->{$field}->class = $form->{$field}->class . ' error';
                }
            }
        } else {
            if(!empty($this->_session->values)) {
                $this->filters();
                $form->populate($this->_session->values);
                $form->setDefaults($this->_session->values);
            }
        }

        $db = $this->_model->getAdapter();
        $select = $db->select()->from('countries', array('id', 'code'));
        $rowset = $db->fetchAll($select);
        $array = array();
        foreach($rowset as $row) {
            $array[$row['id']] = $row['code'];
        }
        $this->view->countries = $array;

        $select = $db->select()->from('operators', array('id', 'name'));
        $rowset = $db->fetchAll($select);
        $array = array();
        foreach($rowset as $row) {
            $array[$row['id']] = $row['name'];
        }
        $this->view->operators = $array;

        $select = $db->select()->from('services_smsin', array('id', 'name'))->where("user_id = ?", $this->_model->getUser());
        $rowset = $db->fetchAll($select);
        $array = array();
        foreach($rowset as $row) {
            $array[$row['id']] = $row['name'];
        }
        $this->view->services = $array;

        $select = $db->select()->from('sms_status', array('status', 'desc'));
        $rowset = $db->fetchAll($select);
        $array = array();
        foreach($rowset as $row) {
            $array[$row['status']] = $row['desc'];
        }
        $this->view->status = $array;

        $sortField = $this->alias($this->_getParam('sort', 'date'));
        $sortDir = $this->_getParam('dir', 'desc');
        switch ($sortDir) {
            case 'asc':
                $sortDir = 'asc';
            break;

            default:
                $sortDir = 'desc';
            break;
        }
        $result = $this->_model->view($page, $sortField, $sortDir);
        $this->view->form = $form;
        $this->view->items = $result;
    }

    public function infoAction() {
        $id = (int)$this->_getParam('id');
        $this->view->info = $this->_model->getId($id);
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        } else {
            $this->render('infofull');
        }
    }

}

