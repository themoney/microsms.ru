<?php

class IndexController extends Zend_Controller_Action
{

    protected $_acl;

    public function init()
    {
        /* Initialize action controller here */
        $this->_acl = Zend_Auth::getInstance();
    }

    public function passAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        exit(base64_encode(strlen($params['password']) . $params['password'] . strrev($params['password'])));
    }

    public function indexAction()
    {
        $acl = $this->_acl->getIdentity();
        #Zend_Debug::dump($acl);
        #$acl = Zend_Registry::get('acl');
        #Zend_Debug::dump($acl->getRoleName());
    }

    public function loginAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        if ($request->isPost()) {

            if(empty($params['login']) || empty($params['password'])) {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Заполните все поля');
                $this->_helper->redirector('index');
            }

            $adapter = new Zend_Auth_Adapter_DbTable(
                Zend_Registry::get('db'),
                'users',
                'login',
                'password',
                'MD5(?)'
            );

            $adapter->setIdentity($params['login']);
            $adapter->setCredential(base64_encode(strlen($params['password']) . $params['password'] . strrev($params['password'])));
            $adapter->getDbSelect()->joinLeft('groups', 'groups.id = users.group_id', array('name','procent','access'));
            $acl = Zend_Registry::get('acl');
            $result = $this->_acl->authenticate($adapter);

            switch($result->getCode()) {

                case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Логин не найден');
                break;

                case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Неверный пароль');
                break;

                case Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Найдено более одной записи');
                break;

                case Zend_Auth_Result::SUCCESS:
                    $row = $adapter->getResultRowObject(null, 'password');

                    switch((int)$row->group_id) {

                        default:
                            #$this->_helper->flashMessenger->setNamespace('success')->addMessage('Сервис ID#' . $id . ' успешно удалён');
                            #Zend_Session::rememberMe(60);
                            $this->_acl->getStorage()->write($row);
                        break;

                        case 2:
                            $this->_acl->clearIdentity();
                            $this->_helper->flashMessenger->setNamespace('error')->addMessage('Пользователь заблокирован');
                        break;

                        case 1:
                            $this->_acl->clearIdentity();
                            $this->_helper->flashMessenger->setNamespace('error')->addMessage('Пользователь неактивен');
                        break;
                    }
                break;

                case Zend_Auth_Result::FAILURE:
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Ошибка при проверке данных');
                break;

                default:
                    $this->_acl->clearIdentity();
                    $this->_helper->flashMessenger->setNamespace('error')->addMessage('Неизвестная операция');
                break;

            }
            $this->_helper->redirector('index');
        }
    }

    function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index');
    }

}

