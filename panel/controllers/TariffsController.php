<?php

class TariffsController extends Zend_Controller_Action
{

    public function init()
    {
        #$acl = Zend_Auth::getInstance();
        #if(!$acl->hasIdentity()) { throw new Zend_Controller_Action_Exception('Тарифы для не найдены', 404); }
    }

    public function indexAction() {
        $this->view->country = $this->_getParam('country', 'ru');
        $this->view->number = $this->_getParam('num', 1132);
        #Zend_Debug::dump($this->_getAllParams());
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from('countries')->order('name ASC');
        $this->view->countries = $db->fetchAll($select);
        $select = $db->select();
        $select->from('numbers')->order('number ASC');
        $this->view->numbers = $db->fetchAll($select);

        $select = $db->select();
        $select->from('countries')->where('code = ?', $this->view->country)->limit(1);
        $c = $db->fetchRow($select);
        $this->view->country = $c['name'];
        $procent = (float)Zend_Auth::getInstance()->getIdentity()->procent;

        $select = $db->select();
        $select->from('tariffs', array('tariffs.*', 'partner' => 'ROUND(partner_income / 100 * ' . $procent . ',2)', 'operators.name'))
                ->joinLeft('operators', 'operators.id = tariffs.operator_id')
                ->where('country_id = ?', $c['id'])
                ->where('number = ?', (int)$this->view->number)
                ->order('priority ASC');
        $this->view->tariffs = $db->fetchAll($select);
    }

}

