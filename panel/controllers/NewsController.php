<?php

class NewsController extends Zend_Controller_Action
{
    protected $news;

    public function init()
    {
        $this->view->headTitle('Новости');
        $this->news = new Core_Model_News();
    }

    public function indexAction()
    {
        $page = $this->_getParam('page', 1);
        $this->view->headTitle()->append('Страница ' . $page);
        $this->view->news = $this->news->page($page);
    }

    public function itemAction()
    {
        $id = $this->_getParam('id', 1);
        $news = $this->news->item($id);
        $this->view->headTitle()->append($news->title);
        $this->view->news = $news;
    }

}

