<?php

class StatsController extends Zend_Controller_Action
{

    protected $_session;

    protected $_model;

    protected $_acl;

    public function init() {
        $this->_session = new Zend_Session_Namespace('Form_Stats');
        $this->_model = new Core_Model_Summarystats();
        $this->_acl = Zend_Auth::getInstance();
        $this->_model->setUser((int)$this->_acl->getIdentity()->id);
    }

    protected function buildDate($m, $params) {
        return $params[$m . 'y'] . '-' . $params[$m . 'm'] . '-' . $params[$m . 'd'] . ' ' . (($m=='to') ?  '23:59:59' : '00:00:00');
    }

    protected function filters() {
        if(!empty($this->_session->values['country']))
            $this->_model->setFilter('country_id = ?', $this->_session->values['country']);
        if(!empty($this->_session->values['operator']))
            $this->_model->setFilter('operator_id = ?', $this->_session->values['operator']);
        if(!empty($this->_session->values['number']))
            $this->_model->setFilter('number = ?', $this->_session->values['number']);
        if(!empty($this->_session->values['service']))
            $this->_model->setFilter('service = ?', $this->_session->values['service']);
        if(!empty($this->_session->values['prefix']))
            $this->_model->setFilter('prefix = ?', $this->_session->values['prefix']);
        if(!empty($this->_session->values['abonent']))
            $this->_model->setFilter('abonent LIKE ?', $this->_session->values['abonent'] . "%");
        if(!empty($this->_session->values['datefrom']['fromy'])
            && !empty($this->_session->values['datefrom']['fromm'])
            && !empty($this->_session->values['datefrom']['fromd'])) {
            $from = $this->buildDate('from', $this->_session->values['datefrom']);
            $this->_model->setFilter('date >= ?', $from);
        }
        if(!empty($this->_session->values['dateto']['toy'])
            && !empty($this->_session->values['dateto']['tom'])
            && !empty($this->_session->values['dateto']['tod'])) {
            $to = $this->buildDate('to', $this->_session->values['dateto']);
            $this->_model->setFilter('date <= ?', $to);
        }
    }

    public function indexAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $form = $this->_model->getForm();
        if ($request->isPost()) {
            $this->_session->values = $params;
            if ($form->isValid($this->_session->values)) {
                $this->filters();
            } else {
                $errors = $form->getErrors();
                foreach($errors as $field => $a) {
                    if(count($a)) $form->{$field}->class = $form->{$field}->class . ' error';
                }
            }
        } else {
            if(!empty($this->_session->values)) {
                $this->filters();
                $form->populate($this->_session->values);
                $form->setDefaults($this->_session->values);
            }
        }
        $result = $this->_model->view();
        $this->view->form = $form;
        $this->view->items = $result;
    }


}

