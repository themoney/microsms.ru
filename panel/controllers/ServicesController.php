<?php

class ServicesController extends Zend_Controller_Action
{

    protected $_model;

    protected $_acl;

    public function init()
    {
        /* Initialize action controller here */
        $this->_model = new Core_Model_Services();
        $this->_acl = Zend_Auth::getInstance();
        $this->_model->setUser((int)$this->_acl->getIdentity()->id);
    }

    public function indexAction()
    {
        $sortField = $this->_getParam('sort', 'id');
        $sortDir = $this->_getParam('dir', 'asc');
        switch ($sortDir) {
            case 'asc':
                $sortDir = 'asc';
            break;

            default:
                $sortDir = 'desc';
            break;
        }
        $result = $this->_model->view($sortField, $sortDir);
        $this->view->items = $result;
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        if($request->isPost()) {
            $row = $this->_model->createRow();
            $row->setFromArray($params);
            $row->user_id = (int)$this->_acl->getIdentity()->id;
            $row->type = '';
            $row->status = 'active';
            $row->save();
            $this->_helper->flashMessenger->setNamespace('success')->addMessage('Сервис успешно создан');
            $id = $this->_model->getAdapter()->lastInsertId();
            $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => Zend_Session::getId()));
        }
        $form = $this->_model->getForm();
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->_request->getParam('id', false);
        $hash = $this->_request->getParam('hash', false);
        if($id !== false && $hash == Zend_Session::getId()) {
            if($this->_model->remove($id)) {
                $this->_helper->flashMessenger->setNamespace('success')->addMessage('Сервис ID#' . $id . ' успешно удалён');
            } else {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Сервис ID#' . $id . ' не найден');
            }
        } else {
            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах');
        }
        $this->_helper->redirector('index');
    }

    public function editAction()
    {
        $id = $this->_request->getParam('id', false);
        $hash = $this->_request->getParam('hash', false);
        if($id !== false && $hash == Zend_Session::getId()) {
            if($row = $this->_model->find($id)) {
                $row = $row->current();
                $request = $this->getRequest();
                $params = $request->getParams();
                if($request->isPost()) {
                    /*
                     * Работа с префиксами если они заданы
                     */
                    # Создание
                    $db = Zend_Registry::get('db');
                    if(!empty($params['prefix2']) && (strlen($params['prefix2']) < 3 || empty($params['prefix1']))) {
                        $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах префикса');
                        $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
                    } elseif(!empty($params['prefix2'])) {
                        $select = $db->select();
                        $select->from('prefix__1', array('prefix'))->where('id = ?', (int)$params['prefix1'])->limit(1);
                        $prefix1 = $db-> fetchRow($select);
                        $select = $db->select();
                        $select->from('prefixes',array(
                                "c" => new Zend_Db_Expr('count(`prefix`)')
                            ))
                            ->where("available = ?", 1)
                            ->where("prefix = ?", new Zend_Db_Expr("LEFT(" . strtolower($prefix1['prefix'] . $params['prefix2']) . ",CHAR_LENGTH(prefix))"))
                            ->limit(1);
                        $p = $db-> fetchRow($select);
                        if((bool)$p['c']) {
                            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Префикс недоступен или занят');
                            $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
                        } else {
                            $db->insert('prefix__2', array(
                                'user_id'       => (int)$this->_acl->getIdentity()->id,
                                'service_id'    => (int)$id,
                                'prefix_id'     => (int)$params['prefix1'],
                                'prefix'        => strtolower($params['prefix2'])
                            ));
                            $this->_helper->flashMessenger->setNamespace('success')->addMessage('Новый префикс успешно добавлен');
                        }
                    }
                    # Удаление
                    if(!empty($params['prefixes']) && is_array($params['prefixes'])) {
                        $db->delete('prefix__2', $db->quoteInto('id IN (?)', $params['prefixes']));
                        $this->_helper->flashMessenger->setNamespace('success')->addMessage('Выбранные префиксы успешно удалены');
                    }
                    /*
                     * Сохранение значений сервиса
                     */
                    $row->setFromArray($params);
                    $row->save();
                    $this->_helper->flashMessenger->setNamespace('success')->addMessage('Сервис успешно изменён');
                    $this->_helper->redirector('edit', 'services', null, array('id' => $id, 'hash' => $hash));
                }
                $form = $this->_model->getForm($row);
                $this->view->form = $form;
            } else {
                $this->_helper->flashMessenger->setNamespace('error')->addMessage('Сервис ID#' . $id . ' не найден');
                $this->_helper->redirector('index');
            }
        } else {
            $this->_helper->flashMessenger->setNamespace('warning')->addMessage('Ошибка в переданных параметрах');
            $this->_helper->redirector('index');
        }
    }

    public function freeAction() {
        /*
            CREATE
             ALGORITHM = UNDEFINED
             VIEW `prefixes`
             (prefix, service_id, user_id, available)
             AS SELECT CONCAT(`prefix__1`.`prefix`, `prefix__2`.`prefix`) as 'prefix', `prefix__2`.`service_id`, `prefix__2`.`user_id`,`prefix__1`.`available` FROM `prefix__2` LEFT JOIN `prefix__1` ON `prefix__1`.`id` = `prefix__2`.`prefix_id`
        */
        $prefix1 = $this->_request->getParam('prefix1', false);
        $prefix2 = $this->_request->getParam('prefix2', false);
        if(empty($prefix1) || empty($prefix2) || strlen($prefix2) < 3) {
            $row['c'] = 1;
        } else {
            $db = Zend_Registry::get('db');
            $select = $db->select();
            /*$select->from('prefix__2', array(
                    "c" => new Zend_Db_Expr('count(*)')
                ))
                ->where(new Zend_Db_Expr("CONCAT((SELECT `prefix__1`.`prefix` FROM `prefix__1` WHERE `prefix__1`.`id` = prefix__2.prefix_id), prefix)") . " LIKE ?", strtolower($prefix1 . $prefix2) . "%")
                ->limit(1);
            */
            $select->from('prefixes',array(
                    "c" => new Zend_Db_Expr('count(`prefix`)')
                ))
                ->where("available = ?", 1)
                ->where("prefix = ?", new Zend_Db_Expr("LEFT('" . strtolower($prefix1 . $prefix2) . "',CHAR_LENGTH(prefix))"))
                ->limit(1);
            # 1, Получить префикс 1ур + 2ур
            # 2. Узнать длину префикс LENGTH(1ур + 2ур)
            # 3. Обрезать входящую строку до этого размера SUBSTRING(string, 0, len)
            # 4. Сравнить получившуюся обрезаную строку с 1ур + 2ур
            # 5, Вывести результат совпадения в 1 байт 0/1
            #Zend_Debug::dump($select->__toString());
            #$row['c'] = 0;
            $row = $db-> fetchRow($select);
        }
        $this->_helper->json(array('result' => $row['c']), array(
            'enableJsonExprFinder' => true,
            'keepLayouts'          => true,
        ));
    }

}

