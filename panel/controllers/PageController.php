<?php
class PageController extends Zend_Controller_Action
{

    public function __call($page, $args) {
        require_once 'Zend/Controller/Action/Exception.php';
        try {
            $page = $this->_getParam('page', substr($page, 0, strlen($page) - 6));
            sleep(3);
            echo $this->render($page);
        } catch(Zend_Exception $e) {
            throw new Zend_Controller_Action_Exception(sprintf('Страница "%s" не найдена', $page), 404);
        }
    }

    public function contactsAction() {
        $captcha = new Zend_Captcha_Image();
		$options = array(
		    'missingMessage' => "Поле '%field%' обязательно для заполнения",
            'notEmptyMessage' => "Поле %field% не может быть пустым"
		);
 		$filters = array(
		    'name'   => 'StripTags',
		    'contact' => 'StripTags',
		 	'text' => 'StripTags'
		);

		$validators = array(
		    'name' => "NotEmpty",
            'contact' => "NotEmpty",
			'text' => "NotEmpty"
		);
        $params = $this->_getAllParams();
		$input = new Zend_Filter_Input($filters, $validators, $params, $options);
		if ($input->isValid()) {
			$this->view->error = array('' => 'Ваше сообщение отправлено!');
			return true;
		} else {
			if ($input->hasInvalid() || $input->hasMissing()) {
			 $this->view->error = $input->getMessages();
			 return false;
			}
			$this->view->error = array("" => "Ошибка проверки формы");
		}
    }

}

