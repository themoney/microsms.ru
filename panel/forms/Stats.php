<?php

class Core_Form_Stats extends Zend_Form
{

    public function init()
    {

    }

    public function initForm($user_id, $detail = false) {
        $this->setMethod('post');
        $this->setElementDecorators(array(
            //array('PrepareElements'),
            array('ViewHelper'),
            //array('Errors', array('tag' => 'div')),
            array('Description', array('tag' => 'label')),
            array('Label', array('class' => 'desc'))
            //array('HtmlTag', array('tag' => 'span', 'class' => 'full')),
        ));

        $this->addElement(
            'text', 'fromd', array(
            'id'    => 'datefrom_d',
            'belongsTo' => 'datefrom',
            'class' => 'text',
            'description' => 'День',
            'maxlength' => 2,
            'size'      => 2,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'fromm', array(
            'id'    => 'datefrom_m',
            'belongsTo' => 'datefrom',
            'class' => 'text',
            'description' => 'Месяц',
            'maxlength' => 2,
            'size'      => 2,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'fromy', array(
            'id'    => 'datefrom_y',
            'belongsTo' => 'datefrom',
            'class' => 'text',
            'description' => 'Год',
            'maxlength' => 4,
            'size'      => 4,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'tod', array(
            'id'    => 'dateto_d',
            'belongsTo' => 'dateto',
            'class' => 'text',
            'description' => 'День',
            'maxlength' => 2,
            'size'      => 2,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'tom', array(
            'id'    => 'dateto_m',
            'belongsTo' => 'dateto',
            'class' => 'text',
            'description' => 'Месяц',
            'maxlength' => 2,
            'size'      => 2,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'toy', array(
            'id'    => 'dateto_y',
            'belongsTo' => 'dateto',
            'class' => 'text',
            'description' => 'Год',
            'maxlength' => 4,
            'size'      => 4,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'abonent', array(
            'label' => 'Абонент',
            'class' => 'text',
            'description' => 'Выборка по номеру или части номера',
            //'required' => true,
            'filters'    => array('StringTrim')
        ));

        $this->addElement('button', 'submit', array(
            'ignore'   => true,
            'order'    => -1,
            'label'    => 'Показать',
        ));

        $db = Zend_Registry::get('db');

        // Генерируем список сервисов
        $this->addElement('select', 'service', array(
            'label' => 'Сервис',
            'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        #$select->setIntegrityCheck(false);
        $select->from('services_smsin', array('id', 'name'))
                ->where("user_id = ?", $user_id)
                ->order("name ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->service->addMultiOption($row['id'], "[" . $row['id'] . "] " . $row['name']);

        // Генерируем список операторов
        $this->addElement('select', 'operator', array(
            'label' => 'Оператор',
            'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        $select->from('operators', array('id', 'name'))
                ->order("name ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->operator->addMultiOption($row['id'], $row['name']);

        // Генерируем список стран
        $this->addElement('select', 'country', array(
            'label' => 'Страна',
            'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        $select->from('countries', array('id', 'name'))
                ->order("name ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->country->addMultiOption($row['id'], $row['name']);

        // Генерируем список коротких номеров
        $this->addElement('select', 'number', array(
            'label' => 'Короткий номер',
            'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select()
                ->from('numbers', array('number'))
                ->order("number ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->number->addMultiOption($row['number'], $row['number']);

        // Генерируем список префиксов
        $this->addElement('select', 'prefix', array(
            'label' => 'Префикс',
            'class' => 'select',
            'multiOptions' => array('' => '--- ВСЕ ---')
        ));
        $select = $db->select();
        $select->from('prefix__2', array(
                    "prefix" => new Zend_Db_Expr('CONCAT((SELECT `prefix__1`.`prefix` FROM `prefix__1` WHERE `prefix__1`.`id` = prefix__2.prefix_id), prefix)'))
                )
                ->where("user_id = ?", $user_id)
                ->order("prefix ASC");
        $rowset = $db->fetchAll($select);
        foreach ($rowset as $row) $this->prefix->addMultiOption($row['prefix'], $row['prefix']);

        if($detail) {

            // Генерируем список статусов смс
            $this->addElement('MultiCheckbox', 'status', array(
                'label' => 'Статусы',
                'class' => 'field checkbox',
                'separator' => ''
            ));
            $select = $db->select();
            #$select->setIntegrityCheck(false);
            $select->from('sms_status', array('status', 'desc'))
                    ->order("id ASC");
            $rowset = $db->fetchAll($select);
            foreach ($rowset as $row) $this->status->addMultiOption($row['status'], $row['desc']);

            $this->addElement(
                'text', 'textsms', array(
                'label' => 'Текст sms',
                'class' => 'text',
                'description' => 'Поиск по тексту во входящих sms',
                //'required' => true,
                'filters'    => array('StringTrim')
            ));

        }

    }


}

