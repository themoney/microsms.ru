<?php

class Core_Form_Services extends Zend_Form
{

    public function init()
    {

    }

    public function initForm($user_id, $edit = false) {
        $this->setMethod('post');
        $this->setElementDecorators(array(
            //array('PrepareElements'),
            array('ViewHelper'),
            //array('Errors', array('tag' => 'div')),
            array('Description', array('tag' => 'label')),
            array('Label', array('class' => 'desc'))
            //array('HtmlTag', array('tag' => 'span', 'class' => 'full')),
        ));

        $this->addElement(
            'text', 'name', array(
            'class' => 'text full',
            'label' => 'Название сервиса',
            'description'  => 'Название помогает вам отличать между собой сервисы',
            'maxlength' => 45,
            'required' => true,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'desc', array(
            'label' => 'Описание',
            'class' => 'text full',
            'description' => 'Описание нужно для менеджера',
            'required' => true,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'url', array(
            'label' => 'URL',
            'class' => 'text full',
            'description' => 'Адрес до скрипта обработчика',
            'required' => true,
            'filters'    => array('StringTrim')
        ));

        $this->addElement(
            'text', 'skey', array(
            'label' => 'Секретное слово',
            'class' => 'text medium',
            'description' => 'Используется в запросах к скрипту',
            'required' => true,
            'filters'    => array('StringTrim')
        ));

        $this->addElement('select', 'encode', array(
            'label' => 'Кодировка ответа обработчика',
            'class' => 'select small',
            'multiOptions' => array('utf-8' => 'UTF-8', 'windows-1251' => 'WINDOWS-1251')
        ));

        $this->addElement('button', 'submit', array(
            'ignore'   => true,
            'order'    => -1,
            'label'    => 'Создать',
        ));

        if($edit !== false) {

            $this->populate($edit->toArray());

            $db = Zend_Registry::get('db');

            // Генерируем список ваших префиксов для удаления
            $this->addElement('MultiCheckbox', 'prefixes', array(
                'label' => 'Удалить префиксы',
                'class' => 'field checkbox',
                'separator' => ''
            ));
            $select = $db->select();
            $select->from('prefix__2', array(
                        "id" => "prefix__2.id",
                        "prefix" => new Zend_Db_Expr('CONCAT((SELECT `prefix__1`.`prefix` FROM `prefix__1` WHERE `prefix__1`.`id` = prefix__2.prefix_id), prefix)'))
                    )
                    ->where("user_id = ?", $user_id)
                    ->where("service_id = ?", $edit->id)
                    ->order("prefix ASC");
            $rowset = $db->fetchAll($select);
            #Zend_Debug::dump($select->__toString());
            foreach ($rowset as $row) $this->prefixes->addMultiOption($row['id'], $row['prefix']);

            // Генерируем список префиксов первого уровня
            $this->addElement('select', 'prefix1', array(
                //'label' => 'Префикс',
                //'style' => 'text-align:right;',
                'id'    => 'prefix1',
                'class' => 'select small',
                'multiOptions' => array()
            ));
            $select = $db->select();
            $select->from('prefix__1')
                    ->where("available = ?", 1)
                    ->order("prefix ASC");
            $rowset = $db->fetchAll($select);
            foreach ($rowset as $row) $this->prefix1->addMultiOption($row['id'], $row['prefix']);

            # Поле для второго уровня
            $this->addElement(
                'text', 'prefix2', array(
                //'label' => 'Субпрефикс',
                'id'    => 'prefix2',
                'class' => 'text medium',
                //'style' => 'width:300px',
                //'description' => 'Уникальное продолжение',
                //'required' => true,
                'filters'    => array('StringTrim')
            ));

        }

    }

}

