<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initSession()
    {
        Zend_Session::start();
        #Zend_Session::rememberMe(3600);
        Zend_Registry::set('session', new Zend_Session_Namespace('Site'));
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('Session', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
    }

    /*protected function _initConfig()
    {
        Zend_Registry::set('config', new Zend_Config($this->getOptions()));
    }

    protected function _initCache()
    {
        $options = $this->getOptions();
        $cache = Zend_Cache::factory(
            $options['cache']['frontend']['adapter'],
            $options['cache']['backend']['adapter'],
            $options['cache']['frontend']['params'],
            $options['cache']['backend']['params']
        );
        if((bool)$options['cache']['isDefaultMetadataCache']) Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        if((bool)$options['cache']['isDefaultTranslateCache']) Zend_Translate::setCache($cache);
        if((bool)$options['cache']['isDefaultLocaleCache']) Zend_Locale::setCache($cache);
        if((bool)$options['cache']['isRegistrySet']) Zend_Registry::set('cache', $cache);
        Zend_Wildfire_Plugin_FirePhp::getInstance()->send('Cache', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $cache;
    }*/

    protected function _initDb()
    {
        try {
            $config = $this->getOptions();
            $db = Zend_Db::factory($config['database']['adapter'], $config['database']['params']);
            Zend_Db_Table::setDefaultAdapter($db);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        Zend_Registry::set('db', $db);
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('MySQL', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $db;
    }

    /*protected function _initLogger()
    {
        try {
            $this->bootstrap('db');
        #$writer = new Zend_Log_Writer_Firebug();
        #$logger = new Zend_Log ($writer);
        $columnMapping = array(
        	'priority'     => 'priority',
        	'priorityName' => 'priorityName',
        	'message'      => 'message',
            'timestamp'    => 'timestamp',
        	'ip'           => 'ip',
        	'controller'   => 'controller',
        	'action'       => 'action'
        );
        $writer = new Zend_Log_Writer_Db(Zend_Registry::get('db'), 'logs', $columnMapping);
        $logger = new Zend_Log($writer);
        Zend_Registry::set("log", $logger);
        #Zend_Controller_Action_HelperBroker::addPrefix("Core_Controller_Action_Helper");
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    public function _initZendValidateTranslator() {
        $translator = new Zend_Translate(
            'array',
            APPLICATION_PATH . '/../resources/languages',
            'ru',
            array('scan' => Zend_Translate::LOCALE_DIRECTORY)
        );
        Zend_Validate_Abstract::setDefaultTranslator($translator);
        return $translator;
    }*/

    protected function _initView()
    {
        $config = $this->getOptions();
        $view = new Zend_View();
        $view->doctype($config['resources']['layout']['doctype']);
        $view->headTitle()->setSeparator($config['resources']['view']['titleSeparator'])->set($config['resources']['view']['title']);
        $view->headMeta()->appendName('description', $config['resources']['view']['description']);
        $view->headMeta()->appendName('keywords', $config['resources']['view']['keywords']);
        $view->headMeta()->appendHttpEquiv('Content-Type', $config['resources']['view']['contentType']);
        $view->headMeta()->appendHttpEquiv('X-UA-Compatible','IE=EmulateIE7');
        #$view->headLink()->appendStylesheet('http://cashexpress.me/css/style.css?v=' . VER);
        $view->headLink()->appendStylesheet('/css/style.css');
        #$view->headLink()->appendStylesheet('/css/compassdatagrid.css');
        $view->headScript()->appendFile("https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js");
        #$view->headScript()->appendFile('/js/jquery.livequery.min.js');
        #$view->headScript()->appendFile('/js/jquery.compassdatagrid.min.js');
        #$view->headScript()->appendFile('/js/jquery.detailsRow.js');
        #$view->headScript()->appendFile('/js/functions.js');
        #Zend_Wildfire_Plugin_FirePhp::getInstance()->send('View', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        return $view;
    }

    /*protected function __initDevelopmentBar()
    {
    	try {
		    if(APPLICATION_ENV !== 'development') return;
        	$autoloader = Zend_Loader_Autoloader::getInstance();
	        $autoloader->registerNamespace('Core');
            $frontController = Zend_Controller_Front::getInstance();
            $frontController->registerPlugin(new Core_Controller_Plugin_Developmentbar());
            Zend_Wildfire_Plugin_FirePhp::getInstance()->send('DevBar', 'Init', Zend_Wildfire_Plugin_FirePhp::INFO);
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }*/
}
