<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $news = new Core_Model_News();
        $this->view->news = $news->last();
        $countries = new Core_Model_Countries();
        $this->view->countries = $countries->getCountries();
    }

    public function redirectAction()
    {
        $this->_redirect('http://panel.' . $this->getRequest()->getHttpHost() . '/#' . $this->getRequest()->getParam('to','login'));
    }

}

