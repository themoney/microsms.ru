<?php

class Core_Model_Numbers extends Zend_Db_Table_Abstract
{
    protected $_name = 'numbers';

    protected $_dependentTables = array('Core_Model_Countries');

    protected $_referenceMap    = array(
        'country' => array(
            'columns'           => array('country_id'),
            'refTableClass'     => 'Core_Model_Countries',
            'refColumns'        => array('id')
        )
    );

    public function init() {
                
    }

    /*
     * Получение список стран
     * @return object countries rows
     */
    public function getNumbers($country_id) {

        return $this->fetchAll(array('country_id = ?' => (int)$country_id), 'number ASC');
    }

}