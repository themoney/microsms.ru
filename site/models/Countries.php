<?php

class Core_Model_Countries extends Zend_Db_Table_Abstract
{
    protected $_name = 'countries';

    public function init() {
                
    }

    /*
     * Получение последних новостей
     * @param int $count - кол-во последних новостей
     * @return class rowset
     *
    public function last($count = 5) {
        return $this->fetchAll(null, 'id DESC', $count);
    }

    /*
     * Получение новостей постранично
     * @param int $page - номер страницы
     * @return array news
     *
    public function page($page = 1) {
        $adapter = new Zend_Paginator_Adapter_DbSelect($this->select()->order('id DESC'));
        $paginator = new Zend_Paginator($adapter);
        $paginator->setItemCountPerPage($this->limitPerPage);
        $paginator->setCurrentPageNumber($page);
        #Zend_Debug::dump($paginator); exit();
        return $paginator;
    }
     */
    public function getCountryName($country)
    {
        return $this->fetchRow(array('code' => $country));
    }

    /*
     * Получение список стран
     * @return object countries rows
     */
    public function getCountries() {
        return $this->fetchAll(null, 'name ASC');
    }
}