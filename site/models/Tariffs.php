<?php

class Core_Model_Tariffs extends Zend_Db_Table_Abstract
{
    protected $_name = 'tariffs';

    protected $_dependentTables = array('Core_Model_Operators');

    protected $_referenceMap    = array(
        'operator' => array(
            'columns'           => array('operator_id'),
            'refTableClass'     => 'Core_Model_Operators',
            'refColumns'        => array('id')
        )
    );

    /*
     * Получение список тарифов
     * @return object tariffs rows
     */
    public function getTariffs($country_id, $number, $procent = 85)
    {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('A' => $this->_name), array('A.*', 'partner' => 'ROUND(partner_income / 100 * ' . $procent . ',2)', 'operators.name'))
                ->joinLeft('operators', 'operators.id = A.operator_id')
                ->where('country_id = ?', (int)$country_id)
                ->where('number = ?', (int)$number)
                ->order('priority ASC');
        return $this->fetchAll($select);
    }
}