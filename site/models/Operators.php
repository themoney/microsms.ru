<?php

class Core_Model_Operators extends Zend_Db_Table_Abstract
{
    protected $_name = 'operators';

    protected $_dependentTables = array('Core_Model_Countries');

    protected $_referenceMap    = array(
        'country' => array(
            'columns'           => array('country_id'),
            'refTableClass'     => 'Core_Model_Countries',
            'refColumns'        => array('id')
        )
    );

    public function init() {
                
    }

}